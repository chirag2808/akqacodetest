﻿
namespace akqa.Foundation.ORM.Models
{
    public static class Constants
    {
        public const string ImageTemplateIdString = "D7545FD6-8F87-4DD2-9D41-1D0320E5BD1F";
        public const string Image_TitleFieldName = "Title";
        public const string Image_MediaImageFieldName = "Media Image";
        public const string Image_DescriptionFieldName = "Description";
        public const string Image_MediaLinkFieldName = "Media Link";

        public const string ImageCarouselTemplateIdString = "906F408A-EAC5-4A6D-BCE9-230887E2075A";
        public const string ImageCarousel_HeadingFieldName = "Heading";
        public const string ImageCarousel_ImageCarouselFieldName = "ImageCarousel";
        public const string ImageCarousel_UniqueIdentifierFieldName = "ImageCarousel Unique Identifier";
    }
}