﻿using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.Data;
using System;

namespace akqa.Foundation.ORM.Models.Base
{
    /// <summary>
    /// Base Glass Mapper Model
    /// </summary>
    public interface IGlassBase
    {
        [SitecoreId]
        Guid Id { get; set; }
        [SitecoreInfo(SitecoreInfoType.Language)]
        Sitecore.Globalization.Language Language { get; set; }

        [SitecoreInfo(SitecoreInfoType.Version)]
        int Version { get; set; }
        [SitecoreInfo(SitecoreInfoType.Path)]
        string Path { get; set; }

        [SitecoreInfo(SitecoreInfoType.Name)]
        string Name { get; set; }

        [SitecoreInfo(SitecoreInfoType.DisplayName)]
        string DisplayName { get; set; }

        [SitecoreInfo(SitecoreInfoType.Url)]
        string Url { get; set; }

        [SitecoreInfo(SitecoreInfoType.TemplateId)]
        ID TemplateId { get; set; }
    }
}
