﻿using akqa.Foundation.ORM.Models.Base;
using Glass.Mapper.Sc.Configuration.Attributes;
using System.Collections.Generic;

namespace akqa.Foundation.ORM.Models.ImageModel
{
    [SitecoreType(TemplateId = Constants.ImageCarouselTemplateIdString)]
    public interface IImageCarousel : IGlassBase
    {
        [SitecoreField(Constants.ImageCarousel_ImageCarouselFieldName)]
        IEnumerable<IImageModel> ImageCarousel { get; set; }
        [SitecoreField(Constants.ImageCarousel_HeadingFieldName)]
        string Heading { get; set; }
        [SitecoreField(Constants.ImageCarousel_UniqueIdentifierFieldName)]
        string ImageCarousel_UniqueIdentifierFieldName { get; set; }
    }
}
