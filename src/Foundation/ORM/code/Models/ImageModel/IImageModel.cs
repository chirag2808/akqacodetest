﻿using akqa.Foundation.ORM.Models.Base;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;

namespace akqa.Foundation.ORM.Models.ImageModel
{
    [SitecoreType(TemplateId = Constants.ImageTemplateIdString)]
    public interface IImageModel: IGlassBase
    {
        [SitecoreField(Constants.Image_TitleFieldName)]
        string Title { get; set; }
        [SitecoreField(Constants.Image_DescriptionFieldName)]
        string Description { get; set; }
        [SitecoreField(Constants.Image_MediaLinkFieldName)]
        Link MediaLink { get; set; }
        [SitecoreField(Constants.Image_MediaImageFieldName)]
        Image MediaImage { get; set; }
    }
}
