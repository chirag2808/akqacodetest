﻿namespace akqa.Foundation.ORM.Services
{
    public interface IRenderingService
    {
        T GetDataSourceItem<T>() where T : class;
    }
}
