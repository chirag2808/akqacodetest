﻿using Glass.Mapper.Sc.Web.Mvc;

namespace akqa.Foundation.ORM.Services
{
    public class RenderingService : IRenderingService
    {
        private readonly IMvcContext _mvcContext;
        public RenderingService(IMvcContext mvcContext)
        {
            _mvcContext = mvcContext;
        }
        /// <summary>
        /// Used To get Datsource Item using Glass Mapper
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetDataSourceItem<T>() where T : class
        {
            return _mvcContext.GetDataSourceItem<T>();
        }
    }
}