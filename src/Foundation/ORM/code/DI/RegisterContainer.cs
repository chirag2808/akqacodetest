﻿using akqa.Foundation.ORM.Services;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web;
using Glass.Mapper.Sc.Web.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using System;
using System.Linq;

namespace akqa.Foundation.ORM.DI
{
    public class RegisterContainer : IServicesConfigurator
    {
        /// <summary>
        /// Used for Initializing MVC Context and Sitecore Service
        /// </summary>
        /// <param name="serviceCollection"></param>
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ISitecoreService>(provider =>
            {
                var databaseName = Sitecore.Context.Database?.Name.ToLower();
                if (databaseName == null || databaseName == "core")
                {
                    try
                    {
                        if (Sitecore.Configuration.Factory.GetDatabaseNames().Contains("master"))
                            return new SitecoreService("master");
                        else
                            return new SitecoreService("web");
                    }
                    catch (Exception)
                    {
                        return new SitecoreService("web");
                    }
                }
                return new SitecoreService(Sitecore.Context.Database);
            });

            serviceCollection.AddScoped<IRequestContext, RequestContext>();
            serviceCollection.AddScoped<IMvcContext, MvcContext>();
            serviceCollection.AddScoped<IGlassHtml, GlassHtml>();

            serviceCollection.AddScoped(typeof(IRenderingService), typeof(RenderingService));
        }
    }
}