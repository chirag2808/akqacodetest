﻿using akqa.Foundation.ORM.Models.ImageModel;
using akqa.Foundation.ORM.Services;
using System;

namespace akqa.Feature.Carousel.Services
{
    public class CarouselService : ICarouselService
    {
        private readonly IRenderingService _renderingService;
        public CarouselService(IRenderingService renderingService)
        {
            _renderingService = renderingService;
        }
        public IImageCarousel CreateViewModel()
        {
            try
            {
                var dataSourceItem = _renderingService.GetDataSourceItem<IImageCarousel>();
                return dataSourceItem;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Error in CreateViewModel Method of Carousel Service "+ ex,this);
                return null;
            }
        }
    }
}