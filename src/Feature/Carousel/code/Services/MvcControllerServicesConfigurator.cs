﻿using akqa.Feature.Carousel.Controllers;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace akqa.Feature.Carousel.Services
{
    public class MvcControllerServicesConfigurator: IServicesConfigurator
    {
        /// <summary>
        /// Used for Resolving Dependency Injection
        /// </summary>
        /// <param name="serviceCollection"></param>
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient(typeof(CarouselController));
            serviceCollection.AddScoped(typeof(ICarouselService), typeof(CarouselService));
        }
    }
}