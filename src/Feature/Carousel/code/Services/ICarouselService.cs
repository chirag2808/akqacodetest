﻿using akqa.Foundation.ORM.Models.ImageModel;

namespace akqa.Feature.Carousel.Services
{
    public interface ICarouselService
    {
        IImageCarousel CreateViewModel();
    }
}
