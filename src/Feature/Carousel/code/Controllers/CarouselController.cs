﻿using akqa.Feature.Carousel.Services;
using System.Web.Mvc;

namespace akqa.Feature.Carousel.Controllers
{
    public class CarouselController : Controller
    {
        private readonly ICarouselService _carouselService;
        public CarouselController(ICarouselService CarouselService)
        {
            _carouselService = CarouselService;
        }
        public ActionResult Index()
        {
            var viewmodel = _carouselService.CreateViewModel();
            return View(viewmodel);
        }
    }
}