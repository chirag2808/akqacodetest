# akqaCodeTest

Creating an Image Carousel Component with Image, Title, Description and CTA fields in Carousel using TDS and Glass Mapper.

We can take the clone using the following url:

https://gitlab.com/chirag2808/akqacodetest.git


You can sync content items to the Sitecore by changing Sitecore Web Url and Sitecore Deploy Folder at the build tab in the following solutions:

	1. akqa.Website.Master
	2. akqa.Website.Core
	
akqa.Website.Core is used to sync edit frame with sitecore and akqa.Website.Core is used to sync content items,template,media library, layout and renderings.

We need to add host name of local sitecore Environment inside following config file 

	src\Project\AKQA\code\App_Config\Include\Project\akqa.Website.config
	
Here we need to replace the targetHostName property with locally running Sitecore url.

Then in order to deploy dll,config and view file changes , we need to rebuild 'akqa.Website.Master' solution.
	
I have uploaded the video for the demo on youtube. Please find the link below:

	https://youtu.be/BsLD5liOXPk

Prerequisitives:

	We need  to have Sitecore 9.0.2 solution running on local environment.



